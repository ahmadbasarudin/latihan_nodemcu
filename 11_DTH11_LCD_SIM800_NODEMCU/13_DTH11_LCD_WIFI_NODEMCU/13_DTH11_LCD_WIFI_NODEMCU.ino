#include "DHT.h"
#include <ESP8266WiFi.h>

#include <SoftwareSerial.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <Wire.h>  // This library is already built in to the Arduino IDE
#include <LiquidCrystal_I2C.h> //This library you can add via Include Library > Manage Library > 


#define WIFI_AP "macbook"
#define WIFI_PASSWORD "12345678"


#define DHTPIN                          D5
#define RELAY_PIN                       D6


#define GPIO0 D3 //D3
#define GPIO2 D4 //D4
#define GPIO0_PIN D7
#define GPIO2_PIN D8

#define DHTTYPE                         DHT11
#define MODEM_RX                        D8
#define MODEM_TX                        D7
bool    USE_LCD                 =       true;
const int     BAUDRATE          =       9600;

const char*   MQTT_HOST         =       "demo.thingsboard.io";
const int     MQTT_PORT         =       1883;
const char*   MQTT_CLIENT       =       "basardevice";
const char*   MQTT_USER         =       "THIcq1O0O5QjcD598RME";
const char*   MQTT_PASS         =       "";

const char    MQTT_PUB_TOPIC[]  =       "v1/devices/me/telemetry";
const char    MQTT_SUB_TOPIC[]  =       "v1/devices/me/attributes";
//const char    MQTT_SUB_TOPIC[]  =       "'v1/devices/me/rpc/request/+";

#define       MQTT_KEEPALIVE            60


float kelembaban =0;
float suhu = 0;
int status = WL_IDLE_STATUS;

boolean gpioState[] = {false, false};


//waktu yang diperlukan ambil data setiap 1 detik
#define       TIME_FECTH_DATA           2000 
//waktu yang dibutuhkan untuk looping kembali
#define       TIME_LOOP_PROCESS         2000 
#define       MQTT_KEEPALIVE            60

LiquidCrystal_I2C lcd(0x27, 16, 2);
DHT dht(DHTPIN, DHTTYPE);

WiFiClient wifiClient;
PubSubClient client(wifiClient);



void setup()
{
  Serial.begin(BAUDRATE);
  
  
  pinMode(GPIO0, OUTPUT);
  pinMode(GPIO2, OUTPUT);
  delay(10);

  if(USE_LCD)
  {
    //Wire.begin(2,0);
    lcd.init();   // initializing the LCD
    lcd.backlight(); // Enable or Turn On the backlight 
    lcd.print("Memulai... "); // Start 
    Serial.print("menggunakan lcd");
  }
  
  if(USE_LCD)
  {
    lcd.clear();
    lcd.print("menghubungkan AP :"); // Start 
    lcd.setCursor(0,1);
    lcd.print(WIFI_AP);  
  }
  WiFi.begin(WIFI_AP, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(200);
    Serial.print(".");
    if(USE_LCD)
    {
      lcd.print("."); 
    }
    
  }
  Serial.println("");
  Serial.print("connected to ");
  Serial.println(WIFI_AP);
  
  if(USE_LCD)
  {
    lcd.clear();
    lcd.print( WIFI_AP);
    lcd.print( " terhubung");
      
  }
  client.setServer( MQTT_HOST, MQTT_PORT );
  client.setCallback(mqttCallback);
}

void loop()
{
  if ( !client.connected() ) 
  {
      reconnect();
  }
  
  //read_data();
  //delay(TIME_LOOP_PROCESS);
  //publish_data();
  //delay(TIME_LOOP_PROCESS);
  client.loop();
}

void reconnect() 
{
  // Loop until we're reconnected
  while (!client.connected()) {
    status = WiFi.status();
    if ( status != WL_CONNECTED) {
      WiFi.begin(WIFI_AP, WIFI_PASSWORD);
      while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
      }
      Serial.println("Connected to AP");
    }
    Serial.print("Connecting to ");
    Serial.print(String(MQTT_HOST) + "...");
    // Attempt to connect (clientId, username, password)
    if ( client.connect(MQTT_CLIENT, MQTT_USER, MQTT_PASS) ) {
      Serial.println( "[DONE]" );
      client.subscribe(MQTT_SUB_TOPIC);
      if(USE_LCD)
      {
        lcd.clear();
        lcd.print("MQTT terkoneksi");  
      }
    } else {
      Serial.print( "[FAILED] [ rc = " );
      Serial.println( " : retrying in 5 seconds]" );
      delay( 500 );
    }
  }
}

void publish_data()
{
  
  StaticJsonDocument<200> doc;
  char buffer[256];
  
  //data temperatur
  //data kelembaban
  doc["kelembaban"] = kelembaban;
  doc["suhu"] = suhu;
  
  if(USE_LCD)
  {
    lcd.clear();
    lcd.print("publish data ke ");  
    lcd.setCursor(0,1);
    lcd.print(MQTT_HOST);  
  }
  
  size_t n = serializeJson(doc, buffer);
  client.publish(MQTT_PUB_TOPIC, buffer, n);

}

void read_data()
{
  
  kelembaban = dht.readHumidity();
  suhu = dht.readTemperature();
  
  Serial.print("\n");
  Serial.print(F("Humidity: "));
  Serial.print(kelembaban);
  Serial.print("\t");
  Serial.print(F("  Temperature: "));
  Serial.print(suhu);
  Serial.print(F("°C "));

  if(USE_LCD)
  {
    lcd.clear();
      
    lcd.print("Temp:  Humidity:");
    if (isnan(kelembaban) || isnan(suhu)) {
      lcd.print("ERROR");
      return;
    }
    lcd.setCursor(0,1);
    lcd.print(suhu);
    lcd.setCursor(7,1);
    lcd.print(kelembaban); 
    lcd.print("    "); 
  }
  delay(TIME_FECTH_DATA);

}
void mqttCallback(const char* topic, byte* payload, unsigned int length) {

  Serial.println("On message");
  
  char json[length + 1];
  strncpy (json, (char*)payload, length);
  json[length] = '\0';
  
  Serial.print("Topic: ");
  Serial.println(topic);
  Serial.print("Message: ");
  Serial.println(json);
  
  // Decode JSON request
  StaticJsonDocument<200> jsonBuffer;
  //JsonObject& data = jsonBuffer.parseObject((char*)json);
  auto error = deserializeJson(jsonBuffer, (char*)json);
  
  if (error)
  {
  Serial.print(F("deserializeJson() failed with code "));
  Serial.println(error.c_str());
  return;
  }
  
  // Check request method
  String methodName = String((const char*)jsonBuffer["method"]);
  
  if (methodName.equals("getGpioStatus")) {
  // Reply with GPIO status
  String responseTopic = String(topic);
  responseTopic.replace("request", "response");
  client.publish(responseTopic.c_str(), get_gpio_status().c_str());
  } else if (methodName.equals("setGpioStatus")) {
  // Update GPIO status and reply
  set_gpio_status(jsonBuffer["params"]["pin"], jsonBuffer["params"]["enabled"]);
  String responseTopic = String(topic);
  responseTopic.replace("request", "response");
  client.publish(responseTopic.c_str(), get_gpio_status().c_str());
  client.publish("v1/devices/me/attributes", get_gpio_status().c_str());
  }
}

String get_gpio_status() {
// Prepare gpios JSON payload string
  StaticJsonDocument<200> jsonDoc;
  
  jsonDoc[String(GPIO0_PIN)] = gpioState[0] ? true : false;
  jsonDoc[String(GPIO2_PIN)] = gpioState[1] ? true : false;
  char payload[256];
  serializeJson(jsonDoc, payload);
  String strPayload = String(payload);
  Serial.print("Get gpio status: ");
  Serial.println(strPayload);
  return strPayload;
}

void set_gpio_status(int pin, boolean enabled) {
  if (pin == GPIO0_PIN) {
  // Output GPIOs state
  digitalWrite(GPIO0, enabled ? HIGH : LOW);
  // Update GPIOs state
  gpioState[0] = enabled;
  } else if (pin == GPIO2_PIN) {
  // Output GPIOs state
  digitalWrite(GPIO2, enabled ? HIGH : LOW);
  // Update GPIOs state
  gpioState[1] = enabled;
  }
}
