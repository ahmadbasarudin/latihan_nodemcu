
int lamp = LED_BUILTIN;
void setup() {
  // put your setup code here, to run once:

    pinMode(lamp, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
    digitalWrite(lamp, LOW);
    delay(1000);
    digitalWrite(lamp, HIGH);
    delay(1000);

}
