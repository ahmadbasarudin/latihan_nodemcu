#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#ifndef STASSID
#define STASSID "macbook"
#define STAPSK  "12345678"
#endif

const char* ssid     = STASSID;
const char* password = STAPSK;

const char* mqtt_server = "192.168.43.156";
const int mqtt_port = 1883;
const char* mqtt_client_id = "MQTT_CLIENT_ID";


const int lamp = LED_BUILTIN;
boolean wifi_state = false;
int wifi_interval_connecting = 500;

const char* publish_topic = "DEVICE/UNSECURE";
const char* subscribe_topic = mqtt_client_id;


WiFiClient esp_client;
PubSubClient client(esp_client);

void setup() {
  Serial.begin(115200);
  pinMode(lamp, OUTPUT);
  setup_wifi();
  
  setup_wifi();
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  
}


void setup_wifi() 
{
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  /* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default,
     would try to act as both a client and an access-point and could cause
     network-issues with your other WiFi-devices on your WiFi-network. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  digitalWrite(lamp, LOW);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(wifi_interval_connecting);
    Serial.print(".");
    
    wifi_state = !wifi_state; 
    digitalWrite(lamp, wifi_state);
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  
}
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is active low on the ESP-01)
  } else {
    digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  }

}


void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");

    // Attempt to connect
    if (client.connect(mqtt_client_id) )
    {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish(publish_topic, "hello world");
      // ... and resubscribe
      client.subscribe(subscribe_topic);
    } 
    else 
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
void loop() 
{

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}
