#define TINY_GSM_MODEM_SIM800

#include <SoftwareSerial.h>
#include <TinyGsmClient.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

#include <ESP8266WiFi.h>

#include "DHT.h"


#define DHTTYPE DHT11

#define DHTPIN               D5
#define MODEM_RX             D8
#define MODEM_TX             D7
#define MODEM_RESET          D2
#define SOC_RESET            D3

//#define DUMP_AT_COMMANDS
const int   BAUDRATE = 9600;

//const char GPRS_APN[]      = "www.indosat-m3.net"; // APN (example: internet.vodafone.pt) use https://wiki.apnchanger.org
//const char GPRS_USER[] = "gprs"; // GPRS User
//const char GPRS_PASS[] = "im3"; // GPRS 

const char GPRS_APN[]      = "3gprs"; // APN (example: internet.vodafone.pt) use https://wiki.apnchanger.org
const char GPRS_USER[] = "3gprs"; // GPRS User
const char GPRS_PASS[] = "3gprs"; // GPRS 


//const char GPRS_APN[]      = "internet"; // APN (example: internet.vodafone.pt) use https://wiki.apnchanger.org
//const char GPRS_USER[] = "wap"; // GPRS User
//const char GPRS_PASS[] = "wap123"; // GPRS 

//const char* MQTT_HOST = "broker.mqtt-dashboard.com";
//const char* MQTT_HOST = "node02.myqtthub.com";
const char* MQTT_HOST = "broker.emqx.io";
//const char* MQTT_HOST = "34.207.172.255";


const int   MQTT_PORT = 1883;
const char* MQTT_CLIENT = "sim800lv2";
const char* MQTT_USER = "sim800lv2";
const char* MQTT_PASS = "sim800lv2";
const char MQTT_SUB_TOPIC[] = "/basarudin/sekolah";
const char MQTT_PUB_TOPIC[] = "/basarudin/rumah";

//const char MQTT_PUB_SUHU[] = "/basarudin/suhu";
//const char MQTT_PUB_KELEMBAPAN[] = "/basarudin/kelembapan";


//waktu yang diperlukan ambil data setiap 1 detik
#define TIME_FECTH_DATA 1000 
//waktu yang dibutuhkan untuk looping kembali
#define TIME_LOOP_PROCESS 5000 
#define MQTT_KEEPALIVE 60 

float kelembaban =0;
float suhu = 0;
String data = "";

DHT dht(DHTPIN, DHTTYPE);
SoftwareSerial SerialAT(MODEM_RX,MODEM_TX); // RX,TX 
#ifdef DUMP_AT_COMMANDS
  #include <StreamDebugger.h>
  StreamDebugger debugger(SerialAT, Serial);
  TinyGsm modem(debugger);
#else
  TinyGsm modem(SerialAT);
#endif


TinyGsmClient client(modem);
PubSubClient mqtt(client);


boolean mqttConnect()
{
  //if(!mqtt.connect(MQTT_CLIENT,MQTT_USER,MQTT_PASS))
  if(!mqtt.connect(MQTT_CLIENT))
  {
    Serial.print(".");
    return false;
  }
  Serial.println("terkoneksi  ke MQTT broker.");
  mqtt.subscribe(MQTT_SUB_TOPIC);
  return mqtt.connected();
}

void mqttCallback(char* topic, byte* payload, unsigned int len)
{
  
  Serial.print("Pesan diterima: ");
  Serial.write(payload, len);
  Serial.println();

      if( !strncmp((char *)payload, "unlocked", len) )
      {
        Serial.print("Unlocked");
        //digitalWrite(LOCKPIN, LOW); 
        mqtt.publish(MQTT_PUB_TOPIC, "Unlocked");
       } 
      else if( !strncmp((char *)payload, "locked", len)  )
      {
        Serial.print("Locked");
        //digitalWrite(LOCKPIN, HIGH); 
        mqtt.publish(MQTT_PUB_TOPIC, "Locked");

       } 
      else if( !strncmp((char *)payload, "reset", len)  )
      {
        Serial.print("menerima perintah reset");
        modul_reset();

       }                     
}
void setup()
{
  Serial.begin(BAUDRATE);
  
  Serial.println("Memulai sistem.");
  
  SerialAT.begin(BAUDRATE);
  dht.begin();
  modem.restart();

  
  Serial.println("Modem: " + modem.getModemInfo());
  Serial.println("Mencari opertor.");
  
  if(!modem.waitForNetwork(60000L))
  {
    Serial.println("operator tidak terhubung");
    modul_reset();
    while(true);
  }
  
  Serial.println("terhubung dengan operator.");
  Serial.println("Kekuatan sinyal: " + String(modem.getSignalQuality()));
  
  Serial.println("Menghubungkan jaringan GPRS.");
  if (!modem.gprsConnect(GPRS_APN, GPRS_USER, GPRS_PASS))
  {
    Serial.println("GPRS terputus");
    
    modul_reset();
    while(true);
  }
  Serial.println("GPRS sudah terhubung: " + String(GPRS_APN));
  
  mqtt.setServer(MQTT_HOST, MQTT_PORT);
  mqtt.setCallback(mqttCallback);
  Serial.println("Connecting to MQTT Broker: " + String(MQTT_HOST));
  while(mqttConnect()==false) continue;
  Serial.println();

}

void modul_reset()
{
  
  Serial.println("Reset modem...");
  
  modem.restart();
  delay(6000);
  
  Serial.println("Reset SOC...");
  ESP.restart(); //ESP.reset();
}
void read_data()
{
  kelembaban = dht.readHumidity();
  suhu = dht.readTemperature();
  
  Serial.print("\n");
  Serial.print(F("Humidity: "));
  Serial.print(kelembaban);
  Serial.print("\t");
  Serial.print(F("  Temperature: "));
  Serial.print(suhu);
  Serial.print(F("°C "));

  delay(TIME_FECTH_DATA);
}

void publish_data()
{
  
  //mqtt.publish(MQTT_PUB_SUHU,String(suhu).c_str(), true);
  //delay(1000);
  //mqtt.publish(MQTT_PUB_KELEMBAPAN,String(kelembaban).c_str(), true);

  StaticJsonDocument<200> doc;
  char buffer[256];

  //doc["token"] = MQTT_TOKEN;
  //doc["timestamp"] = 0; //belum support timestamp mandiri
  
  //data kelembaban
  doc["sensor-id"] = 1;
  doc["value"] = kelembaban;

  size_t n = serializeJson(doc, buffer);
  mqtt.publish(MQTT_PUB_TOPIC, buffer, n);
  
  //data temperatur
  doc["sensor-id"] = 2;
  doc["value"] = suhu;
  n = serializeJson(doc, buffer);
  mqtt.publish(MQTT_PUB_TOPIC, buffer, n);

}
void loop()
{
  /*
  if(Serial.available())
  {
    delay(10);
    String message="";
    while(Serial.available()) message+=(char)Serial.read();
    mqtt.publish(MQTT_PUB_TOPIC, message.c_str());
  }
  */
  
  read_data();
  publish_data();
  delay(TIME_LOOP_PROCESS);
  if(mqtt.connected())
  {
    mqtt.loop();
    
  }
  else
  {
    //mereset modem dan mikrokontroller
    modul_reset();
  }
}
