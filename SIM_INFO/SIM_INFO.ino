//#define DUMP_AT_COMMANDS

const char gprs_apn[]      = "3gprs"; // APN (example: internet.vodafone.pt) use https://wiki.apnchanger.org
const char gprs_user[] = "3gprs"; // GPRS User
const char gprs_pass[] = "3gprs"; // GPRS Password
//const char gprs_apn[]      = "www.indosat-m3.net"; // APN (example: internet.vodafone.pt) use https://wiki.apnchanger.org
//const char gprs_user[] = "gprs"; // GPRS User
//const char gprs_pass[] = "im3"; // GPRS 

// SIM card PIN (leave empty, if not defined)
const char simPIN[]   = "";

// Your phone number to send SMS: + (plus sign) and country code, for Portugal +351, followed by phone number
// SMS_TARGET Example for Portugal +351XXXXXXXXX
#define SMS_TARGET  "089506555678"

#define CEK_PULSA  "*111*1#"
#define PESAN_SMS  "halo bos"


// Configure TinyGSM library
#define TINY_GSM_MODEM_SIM800      // Modem is SIM800
#define TINY_GSM_RX_BUFFER   1024  // Set RX buffer to 1Kb

#include <TinyGsmClient.h>
#include <SoftwareSerial.h>


// TTGO T-Call pins
#define MODEM_RX             7
#define MODEM_TX             8
#define MODEM_RESET          9


// Range to attempt to autobaud
#define GSM_AUTOBAUD_MIN 9600
#define GSM_AUTOBAUD_MAX 57600


// Set serial for debug console (to Serial Monitor, default speed 115200)
#define SerialMon Serial

SoftwareSerial SerialAT(MODEM_RX, MODEM_TX);  // RX, TX

#ifdef DUMP_AT_COMMANDS
  #include <StreamDebugger.h>
  StreamDebugger debugger(SerialAT, SerialMon);
  TinyGsm modem(debugger);
#else
  TinyGsm modem(SerialAT);
#endif
void setup() {
  
  pinMode(MODEM_RESET, OUTPUT);
  Serial.begin(115200);
  Serial.println("MULAI");
  delay(10);
  
  //TinyGsmAutoBaud(SerialAT, GSM_AUTOBAUD_MIN, GSM_AUTOBAUD_MAX);
  SerialAT.begin(9600);
  
  resetPhone();
  delay(1000);
  String modem_name = modem.getModemName();
  DBG("Modem Name:",modem_name);

  delay(1000);
  String modem_info = modem.getModemInfo();
  DBG("Modem Info:",modem_info);

  
  delay(1000);
  String ussd_balance = modem.sendUSSD(CEK_PULSA);
  DBG("Modem Saldo:",ussd_balance);
  
  
}
void infoGsm()
{
  delay(1000);
  String name = modem.getModemName();
  Serial.print("Modem Name:");
  Serial.println(name);

  delay(1000);
  String modemInfo = modem.getModemInfo();
  Serial.print("Modem Info:");
  Serial.println(modemInfo);
  
  delay(1000);
  String ccid = modem.getSimCCID();
  Serial.print("CCID:");
  Serial.println(ccid);

  delay(1000);
  String imei = modem.getIMEI();
  Serial.print("IMEI:");
  Serial.println(imei);

  delay(1000);
  String imsi = modem.getIMSI();
  Serial.print("IMSI:");
  Serial.println(imsi);

  delay(1000);
  String cop = modem.getOperator();
  Serial.print("Operator:");
  Serial.println(cop);
  
  delay(1000);
  int csq = modem.getSignalQuality();
  Serial.print("Signal quality:");
  Serial.println(csq);

  //modem.sendSMS("089506555678", "some text");
  //modem.sendSMS(SMS_TARGET, PESAN_SMS);
  //Serial.println("Kirim SMS");
  
}
void resetPhone()
{
  
  DBG("Pin HIGH");
  digitalWrite(MODEM_RESET, HIGH);
  delay(5000);
  DBG("Pin LOW");
  digitalWrite(MODEM_RESET, LOW);
  delay(1300);
  digitalWrite(MODEM_RESET, HIGH);
  DBG("Pin HIGH");
}
void loop() {

  
  DBG("Waiting for network...");
  if (!modem.waitForNetwork(600000L)) {
    delay(10000);
    return;
  }

  if (modem.isNetworkConnected()) {
    DBG("terhubung jaringan");
  }

  DBG("Connecting to", gprs_apn);
  if (!modem.gprsConnect(gprs_apn, gprs_user, gprs_pass)) {
    delay(10000);
    return;
  }

  bool res = modem.isGprsConnected();
  if(res)
  {
    DBG("GPRS status: connected" );
  }
  else
  {
    
    DBG("GPRS status: not connected" );
  }
  
  //infoGsm();
  delay(1000);
}
