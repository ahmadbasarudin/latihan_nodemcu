
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>

#include "DHT.h"
#include <ArduinoJson.h>
//https://github.com/bblanchon/ArduinoJson

#ifndef STASSID
#define FIRMWARE_VERSION "1.1"
#define WIFI_SSID "macbook"
#define WIFI_PASSWORD  "12345678"

#define DHT_PIN D7
#define DHT_TYPE DHT11

#define MQTT_TOKEN "9ae979304769b8fd51a5894bcc5e699a"
#define MQTT_ID "MQTT_CLIENT_ID"
#define MQTT_SERVER "192.168.43.156"
#define MQTT_PORT 1883


#define LED_LAMP LED_BUILTIN
//sementara belum support keepalive

//waktu kedip mengkoneksikan ke wifi
#define WIFI_LED_INTERVAL_CONNECTING  500
//waktu yang diperlukan untuk terkoneksi mqtt kembali yang sebelumnya terputus
#define TIME_MQTT_RECONNECTING  5000

//waktu yang diperlukan ambil data setiap 0.5 detik
#define TIME_FECTH_DATA 500 
//waktu yang dibutuhkan untuk looping kembali
#define TIME_LOOP_PROCESS 2000 
#endif





boolean wifi_state = false;

const char* publish_topic = "DEVICE/UNSECURE";
const char* SUBSCRIBE_TASK_TOPIC = strcat("task/" , MQTT_ID);
const char* PUBLISH_TASK_TOPIC = "presence";

WiFiClient esp_client;
PubSubClient client(esp_client);

StaticJsonDocument<200> doc;
char buffer[256];

DHT dht(DHT_PIN, DHT_TYPE);

void setup() 
{
  Serial.begin(115200);
  pinMode(LED_LAMP, OUTPUT);
  setup_wifi();
  
  client.setServer(MQTT_SERVER, MQTT_PORT);
  client.setCallback(callback);
  
}

void ota_update_started() 
{
  Serial.println("CALLBACK:  HTTP update proses dimulai...");
}

void ota_update_finished() 
{
  Serial.println("CALLBACK:  HTTP update proses selesai.");
}

void ota_update_progress(int cur, int total) 
{
  float percent = (float)  ((int)cur * 100) / total ;
  Serial.printf("CALLBACK:  HTTP update proses telah %d dari %d bytes...%5.2f persen \n", cur, total,percent);
}

void ota_update_error(int err) 
{
  Serial.printf("CALLBACK:  HTTP update gagal. kode error %d\n", err);
}


//untuk update ota
void doOtaUpdate(String _url)
{
  
  doc.clear();
  //untuk koneksi mendownload update
  WiFiClient client;
  //biar kelihatan waktu update
  ESPhttpUpdate.setLedPin(LED_BUILTIN, LOW);
  
  // Add optional callback notifiers
  ESPhttpUpdate.onStart(ota_update_started);
  ESPhttpUpdate.onEnd(ota_update_finished);
  ESPhttpUpdate.onProgress(ota_update_progress);
  ESPhttpUpdate.onError(ota_update_error);
  t_httpUpdate_return ret = ESPhttpUpdate.update(client, _url);
  switch (ret) 
  {
      case HTTP_UPDATE_FAILED:
        Serial.printf("HTTP_UPDATE_FAILD Error (%d): %s\n", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
        break;
      case HTTP_UPDATE_NO_UPDATES:
        Serial.println("HTTP_UPDATE_NO_UPDATES");
        break;
      case HTTP_UPDATE_OK:
        Serial.println("HTTP_UPDATE_OK");
        break;
  }
    
    
}
//untuk presensi
void doPresence(bool _state)
{
  doc.clear();

  doc["device-id"] = MQTT_ID;
  doc["timestamp-send"] = 0; //belum support timestamp mandiri
  doc["stace"] = _state;
  size_t n = serializeJson(doc, buffer);
  //client.publish(PUBLISH_TASK_TOPIC, buffer, n);
  client.publish(PUBLISH_TASK_TOPIC, buffer, false);
  Serial.println("telah prensensi  ");
}



void setup_wifi() 
{
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WIFI_SSID);

  /* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default,
     would try to act as both a client and an access-point and could cause
     network-issues with your other WiFi-devices on your WiFi-network. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  digitalWrite(LED_LAMP, LOW);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(WIFI_LED_INTERVAL_CONNECTING);
    Serial.print(".");
    
    wifi_state = !wifi_state; 
    digitalWrite(LED_LAMP, wifi_state);
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  
}
void callback(char* topic, byte* payload, unsigned int length) {
  
 // Pull the value from key "COMMAND" from the JSON message {"task": "presence" , "value" : 200,"other-value":30}
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  
  //jika topiknya adalah task
  if(strcmp(topic,SUBSCRIBE_TASK_TOPIC)==0)
  {
    Serial.print("debug: topik sama");
    Serial.print("debug: tasknya adalah");
    //jika tasknya adalah presence

    
    doc.clear();
    DeserializationError error = deserializeJson(doc, payload);
    if (error) {
      Serial.print("deserializeJson() failed with code: ");
      Serial.println(error.c_str());
    }

    
    const char* task_name = doc["task-name"];
    const char* task_value = doc["task-value"];
    Serial.println(task_name);
    if(strcmp(task_name,"PRESENCE") == 0)
    {
      doPresence(1);
    }
    else if(strcmp(task_name,"OTA-UPDATE") == 0)
    {
      doOtaUpdate(task_value);
    }
  }
}


void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");

    // Attempt to connect
    
    //client.connect(clientId.c_str(),"outTopic",0,1,"Disconnected");
    if (client.connect(MQTT_ID) )
    {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish(publish_topic, "hello world");
      // ... and resubscribe
      client.subscribe(SUBSCRIBE_TASK_TOPIC);
      
      Serial.println();
      Serial.print("resubscribe :");
      Serial.println(SUBSCRIBE_TASK_TOPIC);
    } 
    else 
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(TIME_MQTT_RECONNECTING);
    }
  }
}
void loop() 
{

  if (!client.connected()) 
  {
    reconnect();
  }
  client.loop();
  //read_data();
  //publish_data();
  delay(TIME_LOOP_PROCESS);
}
