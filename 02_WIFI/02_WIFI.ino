
#include <ESP8266WiFi.h>

#ifndef STASSID
#define STASSID "macbook"
#define STAPSK  "12345678"
#endif

const char* ssid     = STASSID;
const char* password = STAPSK;

int lamp = LED_BUILTIN;
boolean wifi_state = false;
int wifi_interval_connecting = 500;

void setup() {
  Serial.begin(115200);
  pinMode(lamp, OUTPUT);

  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  /* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default,
     would try to act as both a client and an access-point and could cause
     network-issues with your other WiFi-devices on your WiFi-network. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  digitalWrite(lamp, LOW);
  while (WiFi.status() != WL_CONNECTED) {
    delay(wifi_interval_connecting);
    Serial.print(".");
    
    wifi_state = !wifi_state; 
    digitalWrite(lamp, wifi_state);

    
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() 
{
}
