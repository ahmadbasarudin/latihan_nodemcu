#define TINY_GSM_MODEM_SIM800

#include <SoftwareSerial.h>
#include <TinyGsmClient.h>
#include <PubSubClient.h>

SoftwareSerial SerialAT(8,7); // RX, TX

#define LOCKPIN 4
#define DUMP_AT_COMMANDS

//Network details
const char apn[]  = "www.indosat-m3.net";
const char user[] = "gprs";
const char password[] = "im3";


// MQTT details

const char* MQTT_HOST = "broker.mqtt-dashboard.com";
const int   MQTT_PORT = 1883;

const char MQTT_SUB_TOPIC[] = "/basarudin/sekolah";
const char MQTT_PUB_TOPIC[] = "/basarudin/rumah";


#ifdef DUMP_AT_COMMANDS
  #include <StreamDebugger.h>
  StreamDebugger debugger(SerialAT, Serial);
  TinyGsm modem(debugger);
#else
  TinyGsm modem(SerialAT);
#endif

//TinyGsm modem(SerialAT);
TinyGsmClient client(modem);
PubSubClient mqtt(client);



void setup()
{
  Serial.begin(9600);
  SerialAT.begin(9600);
  pinMode(LOCKPIN, OUTPUT);
  
  Serial.println("System start.");
  modem.restart();
  Serial.println("Modem: " + modem.getModemInfo());
  Serial.println("Searching for telco provider.");
  if(!modem.waitForNetwork(60000L))
  {
    Serial.println("fail");
    while(true);
  }
  Serial.println("Connected to telco.");
  Serial.println("Signal Quality: " + String(modem.getSignalQuality()));

  Serial.println("Connecting to GPRS network.");
  if (!modem.gprsConnect(apn, user, password))
  {
    Serial.println("fail");
    while(true);
  }
  Serial.println("Connected to GPRS: " + String(apn));
  
  mqtt.setServer(MQTT_HOST, MQTT_PORT);
  mqtt.setCallback(mqttCallback);
  Serial.println("Connecting to MQTT Broker: " + String(MQTT_HOST));
  while(mqttConnect()==false) continue;
  Serial.println();
    
}

void loop()
{
  if(Serial.available())
  {
    delay(10);
    String message="";
    while(Serial.available()) message+=(char)Serial.read();
    mqtt.publish(MQTT_PUB_TOPIC, message.c_str());
  }
  
  if(mqtt.connected())
  {
    mqtt.loop();
  }
}

boolean mqttConnect()
{
  if(!mqtt.connect("GsmClientTest"))
  {
    Serial.print(".");
    return false;
  }
  Serial.println("Connected to broker.");
  mqtt.subscribe(MQTT_SUB_TOPIC);
  return mqtt.connected();
}

void mqttCallback(char* topic, byte* payload, unsigned int len)
{
  
  Serial.print("Message receive: ");
  Serial.write(payload, len);
  Serial.println();

      if( !strncmp((char *)payload, "unlocked", len) )
      {
        Serial.print("Unlocked");
        digitalWrite(LOCKPIN, LOW); 
        mqtt.publish(MQTT_PUB_TOPIC, "Unlocked");
       } 
      else if( !strncmp((char *)payload, "locked", len)  )
      {
        Serial.print("Locked");
        digitalWrite(LOCKPIN, HIGH); 
        mqtt.publish(MQTT_PUB_TOPIC, "Locked");

       }                  
}
