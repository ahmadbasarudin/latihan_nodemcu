int analog_pin = A1; // pin arduino yang terhubung dengan pin S modul sensor tegangan
 
float v_modul = 0.0; 
float v_pengukuran = 0.0;
float R1 = 30000.0; //30k
float R2 = 7500.0; //7500 ohm resistor, 
int value = 0;
 
void setup()
{
   pinMode(analog_pin, INPUT);
   Serial.begin(9600);
   Serial.println("mengukur tegangan DC");
   Serial.println("https://www.cronyos.com");
}
 
void loop()
{
   value = analogRead(analog_pin);
   v_modul = (value * 5.0) / 1024.0;
   v_pengukuran = v_modul / (R2/(R1+R2));
 
  Serial.print("Tegangan keluaran modul = ");
  Serial.print(v_modul,2);
  Serial.print("volt");
  Serial.print(", Hasil pengukuran = ");
  Serial.print(v_pengukuran,2);
  Serial.println("volt");
  delay(1000);
}
